import fs from "fs";
import AdmZip from "adm-zip";


export function parseFile(filePath: string, callback: (wordFileContents: string) => void): void {
	// Reject non-Word files
	if (filePath.toLowerCase().endsWith(".doc") || filePath.toLowerCase().endsWith(".docx")) {
		const zip = new AdmZip(filePath);
		const zipEntries = zip.getEntries();

		// Check if file is valid by checking for the content XML file inside the doc archive
		const hasXmlFile = zipEntries.find(entry => {
			return entry.entryName === "word/document.xml";
		});

		if (hasXmlFile !== undefined) {
			const xmlFileContent = zip.readAsText("word/document.xml");
			callback(xmlFileContent);
		}
	} else {
		console.log(`Info: ${filePath} is not a Word file. Skipping...`);
	}
}

export function parseDirectory(directoryPath: string, callback: (wordFileContents: string) => void, recursive = false): void {
	console.log(`\nProcessing directory ${directoryPath}...`);
	const dirOutput = fs.readdirSync(directoryPath, { withFileTypes: true });

	const listOfDirectoryPaths = [] as string[];
	dirOutput.forEach(fileObject => {
		const filePath = `${directoryPath}/${fileObject.name}`;

		// remember a list of directories, so we can iterate them later
		if (fileObject.isDirectory()) {
			listOfDirectoryPaths.push(filePath);
		} else {
			console.log(`Processing ${filePath}...`);
			parseFile(filePath, callback);
		}
	});

	// We seperate out the directories here to implement a flat recursive search through the file tree
	if (recursive) {
		listOfDirectoryPaths.forEach(directoryPath => {
			parseDirectory(directoryPath, callback, recursive);
		});
	}
}
