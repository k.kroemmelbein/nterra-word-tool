export function getAllParagraphs(input: string): string[] {
	const regex = /<w:r>\s*?<w:t>(.+?)<\/w:t>\s*?<\/w:r>/gm;

	let result;
	const returnVal = [] as string[];
	while ((result = regex.exec(input)) !== null) {
		returnVal.push(result[1]);
	}

	return returnVal;
}
