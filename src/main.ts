import { Command } from "commander";
import fs from "fs";
// import packageJSON from "./package.json"; // TODO: This causes nested build output

import { parseDirectory, parseFile } from "./io";
import { getAllParagraphs } from "./xml";

const commandLineRunner = new Command();

commandLineRunner.version("0.1.0");
commandLineRunner
	.option("-f, --file <file>", "input file")
	.option("-d, --directory <directory>", "input directory", "./input")
	.option("-r, --recursive", "recursive operation");

commandLineRunner.parse(process.argv);
commandLineRunner.showHelpAfterError();

const options = commandLineRunner.opts();

if (options.directory) {
	if (fs.existsSync(options.directory)) {
		// TODO: trailing slash
		parseDirectory(options.directory, wordFileCallback, options.recursive);
	} else {
		console.error(`Error: directory ${options.directory} does not exist.`);
	}
}

if (options.file) {
	if (fs.existsSync(options.file)) {
		parseFile(options.file, wordFileCallback);
	} else {
		console.error(`Error: file ${options.file} does not exist.`);
	}
}

function wordFileCallback(contents: string) {
	const paragraphs = getAllParagraphs(contents);

	paragraphs.forEach(oneParagraph => {
		console.log(oneParagraph);
	})
}
